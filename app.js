const express = require('express');
const cors = require('cors');
const app = express();
const PORT = 3000;
app.listen(PORT, () => console.log(`Server running on port ${PORT}`));

app.use(express.json());
app.use(cors());

const ciudades = ['Paris', 'Barcelona', 'Barranquilla', 'Montevideo', 'Santiago de chile', 'Mexico DF', 'Nueva York'];
app.get('/ciudades', (req, res, next) => res.json(ciudades.filter(ciudad => ciudad.toLowerCase().includes(req.query.q.toString()))));

let misDestinos = [];
app.get('/my', (req, res, next) => res.json(misDestinos));
app.post('/my', (req, res, next) => {
    console.log(res.body);
    misDestinos.push(req.body.nuevo);
    res.json(misDestinos);
});

app.get('/api/translation', (req, res, next) => res.json([
    {lang: req.query.lang, key: 'HOLA', value: `HOLA ${req.query.lang}`}
]));